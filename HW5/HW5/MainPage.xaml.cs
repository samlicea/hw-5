﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;


// Crtl + K + C ---> Comment out code   
// Crtl + K + U ---> Remove comment out code

namespace HW5
{

    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        ObservableCollection<Pin> pins;

        public MainPage()
        {
            InitializeComponent();


            // Starts map as street view and at different point
            MapView.MapType = MapType.Street;
            MapView.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(32.715736, -117.161087), Distance.FromMiles(1)));

            PickerList();

        }

        private void PickerList()
        {
            pins = new ObservableCollection<Pin>
            {

                // All pins go here
                new Pin
                {
                    Type = PinType.SavedPin,
                    Label = "Liberty Station",
                    Address = "2640 Historic Decatur Rd, San Diego, CA 92106, United States",
                    Position = new Position(32.737713, -117.213530)
                },

                new Pin
                {
                    Type = PinType.SavedPin,
                    Label = "Pesto Italian Craft",
                    Address = "6011 El Cajon Blvd A, San Diego, CA 92115, United States",
                    Position = new Position(32.760644, -117.068065)
                },

                new Pin
                {
                    Type = PinType.SavedPin,
                    Label = "Panama 66",
                    Address = "1450 El Prado, San Diego, CA 92101, United States",
                    Position = new Position(32.731734, -117.150879)
                },

                new Pin
                {
                    Type = PinType.SavedPin,
                    Label = "Breakfast Republic SD",
                    Address = "2730 University Ave, San Diego, CA 92104, United States",
                    Position = new Position(32.748873, -117.134257)
                }


            };

            // Puts pins into the picker
            foreach (Pin item in pins)
            {
                PinPicker.Items.Add(item.Label);
                MapView.Pins.Add(item);
            }

            // Resets index to 0 at the end
            PinPicker.SelectedIndex = 0;
        }




        // changes view to Street
        private void Button_Clicked(object sender, EventArgs e)
        {
            MapView.MapType = MapType.Street;
        }

        // Changes view to Satellite
        private void Button_Clicked_1(object sender, EventArgs e)
        {
            MapView.MapType = MapType.Satellite;
        }

        // What happens when pins are selected
        private void pinPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Gets a copy of pin label 'name'
            var input = (Picker)sender;
            string PickerName = input.SelectedItem.ToString();

            foreach (Pin item in pins)
            {
                // If item of label matches picker name, move to map
                if(item.Label == PickerName)
                {
                    MapView.MoveToRegion(MapSpan.FromCenterAndRadius(item.Position, Distance.FromMiles(1)).WithZoom(17));
                }
            }


        }
    }
}
